// import Vue from 'vue';
// import Vuex from 'vuex';
//
// Vue.use(Vuex);
//
// const state = {
//     email: "",
//     displayName: "",
//     picture: "",
// };
//
// const store = new Vuex.Store({
//     state,
//     getters: {
//         email: state => {
//             return state.email;
//         },
//         displayName: state => {
//             return state.displayName;
//         },
//         picture: state => {
//             return state.picture;
//         },
//     },
//     actions: {
//         setEmail (context, e) {
//             return new Promise((resolve) => {
//                 context.commit('setEmail', e);
//                 resolve()
//             }).catch(() => console.log('Could not set email!'))
//         },
//         setDisplayName (context, dn) {
//             return new Promise((resolve) => {
//                 context.commit('setDisplayName', dn);
//                 resolve()
//             }).catch(() => console.log('Could not set email!'))
//         },
//         setPicture (context, p) {
//             return new Promise((resolve) => {
//                 context.commit('setPicture', p);
//                 resolve()
//             }).catch(() => console.log('Could not set email!'))
//         },
//     },
//     mutations: {
//         setEmail (state, data) {
//             state.email = data;
//         },
//         setDisplayName (state, data) {
//             state.displayName = data;
//         },
//         setPicture (state, data) {
//             state.picture = data;
//         },
//     },
// });
//
// export default store;
