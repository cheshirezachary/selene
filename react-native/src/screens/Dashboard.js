import React from 'react';
import { StyleSheet, Text, View, Image, Alert, TouchableOpacity, TouchableHighlight, TextInput } from 'react-native';

export default class DashboardScreen extends React.Component {

  render() {
    return (
      <View style={styles.main}>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    marginTop: 65,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#2a8ab7'
  },
});
