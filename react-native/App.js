import React from 'react';
import { StyleSheet, Text, View, Image, Alert, TouchableOpacity } from 'react-native';
import { createStackNavigator, } from 'react-navigation';
import DashboardScreen from './src/screens/Dashboard';

class LoginScreen extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.containerTop}>
          <Image
            style={styles.logo}
            source={require('./assets/seleneLogo.png')}
          />
          <Text style={styles.header1}>Selene</Text>
        </View>
        <View style={styles.containerLogin}>
          <Text style={styles.header2}>Manage all your cryptocurrency easily in one place</Text>
          <TouchableOpacity
            onPress={() =>
              navigate('Dashboard')
            }
            style={styles.button1}
          >
            <Text
              style={{textAlign: 'center', color: 'white', fontWeight: 'bold', fontSize: 21}}
            >Get Started</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default createStackNavigator({
  Home: {
    screen: LoginScreen,
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#759BFA',
        //backgroundColor: '#01002C',
        borderColor: '#01002C',
      },
    },
  },
  Dashboard: {
    screen: DashboardScreen,
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#759BFA',
    //backgroundColor: '#01002C',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerTop: {
    position: 'absolute',
    top: '15%'
  },
  logo: {
    height: 120,
    width: 120
  },
  header1: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 34,
    marginTop: '70%'
  },
  header2: {
    textAlign: 'center',
    padding: 44,
    fontWeight: 'bold',
    color: '#8E8E93',
    fontSize: 18,
    lineHeight: 25
  },
  containerLogin: {
    position: 'absolute',
    backgroundColor: 'white',
    height: '45%',
    width: '100%',
    bottom: 0
  },
  button1: {
    width: '70%',
    height: 60,
    backgroundColor: '#6CC1F6',
    marginLeft: 'auto',
    marginRight: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5
  }
});
